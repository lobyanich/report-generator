#ifndef __FORMATTER_IMPL_HPP__
#define __FORMATTER_IMPL_HPP__

//--------------------------------------------------------------------------//

#include "interface/ReportObjectVisitor.hpp"
#include "src/property/PropertyHandler.hpp"
#include <vector>

//--------------------------------------------------------------------------//

class LableInterface;
class TableInterface;
class HorizontalLayoutInterface;

//--------------------------------------------------------------------------//

class Formatter final 
    :   public ReportObjectVisitor
{
public:

    Formatter();

    std::string visit ( const LableInterface & );
    std::string visit ( const TableInterface & );
    std::string visit ( const HorizontalLayoutInterface & );

    void setReportWidth( const Property::ReportWidth & );
    void setReportHeight( const Property::ReportHeight & );

    void setAlignment( const Property::Alignment & );

    void setRowLength( const Property::RowLength & );
    void setRowHeight( const Property::RowHeight & );

    void setRowNumber( const Property::RowNumber & );
    void setColumnNumber( const Property::ColumnNumber & );

    void setTopBorder( const Property::Border::Top & );
    void setBottomBorder( const Property::Border::Bottom & );
    void setLeftBorder( const Property::Border::Left & );
    void setRightBorder( const Property::Border::Right & );

private:

    void resetProperties( const LableInterface & );
    void checkProperties( const LableInterface & );
    void setProperties( const LableInterface & );
    void unSetProperties();

    void
    formatLable( 
            const LableInterface &       _lable
        ,   std::vector< std::string > & _formedLable
        ,   int                          _shift = 0
    ) const;

    void
    formatCell( 
            const LableInterface &       _lable
        ,   std::vector< std::string > & _formedLable
        ,   int                          _shift
        ,   bool                         _firstColumn
    ) const;

    std::string
    formatLine( const std::string & _data ) const;

    std::string
    addAlignment( const std::string & _data ) const;

    void addSpaces( std::vector< std::string > & _data ) const;
    void addNewLines( std::vector< std::string > & _data ) const;

    void adddelimiters( 
            std::vector< std::string > & _data
        ,   const char                   _delim
    ) const;

    std::string
    dataToString( std::vector< std::string > const & _data ) const;

    void
    addBorders( 
            std::string & _data
        ,   bool          _firstColumn
    ) const;

    int calculateTableHeight( int _dataHeight ) const;
    int calculateTableLength( int _dataLength ) const;

private:

    int m_reportWidth;
    int m_reportHeight;

    Property::AlignmentType m_alignment;

    int m_rowLength;
    int m_rowHeight;

    int m_rowNumber;
    int m_columnNumber;

    char m_topBorder;
    char m_bottomBorder;
    char m_leftBorder;
    char m_rightBorder;
};

//--------------------------------------------------------------------------//

#endif // __FORMATTER_IMPL_HPP__