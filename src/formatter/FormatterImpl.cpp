#include "FormatterImpl.hpp"
#include "src/container/LableObject.hpp"
#include "src/container/HorizontalLayoutObject.hpp"
#include "src/container/table/TableObject.hpp"

//--------------------------------------------------------------------------//

Formatter::Formatter()
{
}

//--------------------------------------------------------------------------//

std::string
Formatter::visit ( LableInterface const & _lable )
{
    std::vector < std::string > formedLable( _lable.getHeight() );

    unSetProperties();
    setProperties( _lable );
    m_rowHeight = _lable.getHeight();
    formatLable( _lable, formedLable );

    addNewLines( formedLable );

    return dataToString( formedLable );
}

//--------------------------------------------------------------------------//

std::string
Formatter::visit ( HorizontalLayoutInterface const & _hLayout )
{
    std::vector< const LableInterface * > lables;
    int rowHeight{ 0 };

    _hLayout.forEachData(
        [ & lables, & rowHeight ] ( const LableInterface & _lable )
        {
            lables.push_back( &_lable );
            if( _lable.getHeight() > rowHeight )
                rowHeight = _lable.getHeight();
        }
    );

    std::vector< std::string > formedRows( rowHeight );

    for( auto const & lable : lables )
    {
        unSetProperties();
        setProperties( *lable );
        m_rowHeight = rowHeight;
        formatLable( *lable, formedRows );
        addSpaces( formedRows );
    }

    addNewLines( formedRows );

    return dataToString( formedRows );
}

//--------------------------------------------------------------------------//

std::string
Formatter::visit ( TableInterface const & _table )
{
    std::vector< const LableInterface * > lables;

    _table.forEachData(
        [ & lables ] ( const LableInterface & _lable )
        {
            lables.push_back( &_lable );
        }
    );

    resetProperties( *( lables.front() ) );
    m_rowNumber = _table.getRows();
    m_columnNumber = _table.getColumns();

    auto tableHeight = calculateTableHeight( _table.getHeight());
    auto tableLength = calculateTableLength( _table.getLength());

    std::vector< std::string > formedRows( tableHeight );

    int shift{ 1 };

    for( int i{ 0 }; i < lables.size(); ++i )
    {
        bool firstColumn{ i % _table.getColumns() == 0 };
        resetProperties( *lables[ i ] );

        if( m_rowHeight < _table.getRowHeight( i / _table.getColumns()))
            m_rowHeight = _table.getRowHeight( i / _table.getColumns());
        
        if( m_rowLength < _table.getColumnWidth( i % _table.getColumns()))
            m_rowLength = _table.getColumnWidth( i % _table.getColumns());

        formatCell( *lables[ i ], formedRows, shift, firstColumn );

        if( ( i + 1 ) % _table.getColumns() == 0 )
        {
            if ( tableLength < formedRows[ shift ].length() )
                tableLength = formedRows[ shift ].length();
            
            shift += m_rowHeight;
            formedRows[ shift ] =  std::string( tableLength, m_topBorder );
            shift++;
        }
    }

    formedRows[ 0 ] =  std::string( tableLength, m_topBorder );

    addNewLines( formedRows );

    return dataToString( formedRows );
}

//--------------------------------------------------------------------------//

void
Formatter::formatLable( 
        const LableInterface &       _lable
    ,   std::vector< std::string > & _formedLable
    ,   int                          _shift
) const
{
    std::vector< std::string > rawData;
    _lable.forEachData(
        [ &rawData ] ( std::string const & _data )
        {
            rawData.push_back( _data );
        }
    );

    for( int i{ 0 }; i < m_rowHeight; ++i )
    {
        if( i <= rawData.size() - 1 )
            _formedLable[ i + _shift ] += formatLine( rawData[ i ] );
        else if ( _formedLable.size() > i + _shift )
            _formedLable[ i + _shift ] += std::string( m_rowLength, ' ' );
    }
}

//--------------------------------------------------------------------------//

void
Formatter::formatCell( 
        const LableInterface &       _lable
    ,   std::vector< std::string > & _formedLable
    ,   int                          _shift
    ,   bool                         _firstColumn
) const
{
    formatLable( _lable, _formedLable, _shift );
    for( int i{ 0 }; i < m_rowHeight; ++i )
        addBorders( _formedLable[ i + _shift ], _firstColumn );
}

//--------------------------------------------------------------------------//

void 
Formatter::resetProperties( const LableInterface & _lable )
{
    unSetProperties();
    setProperties( _lable );
    checkProperties( _lable );
}

//--------------------------------------------------------------------------//

void 
Formatter::checkProperties( const LableInterface & _lable )
{
    if ( m_rowLength < _lable.getLength() )
        m_rowLength = _lable.getLength();

    if ( m_rowHeight < _lable.getHeight() )
        m_rowHeight = _lable.getHeight();
}

//--------------------------------------------------------------------------//

void 
Formatter::unSetProperties()
{
    m_reportWidth = 0;
    m_reportHeight = 0;

    m_alignment = Property::AlignmentType::Left;

    m_rowLength = 0;
    m_rowHeight = 0;

    m_rowNumber = 0;
    m_columnNumber = 0;

    m_topBorder = '-';
    m_bottomBorder = '-';
    m_leftBorder = '|';
    m_rightBorder = '|';
}

//--------------------------------------------------------------------------//

void
Formatter::setProperties( const LableInterface & _lable )
{
    PropertyHandler propertyHandler( *this );

    _lable.forEachProperty(
        [ &propertyHandler ] ( const BaseProperty * _property )
        {
            _property->accept( propertyHandler );
        }
    );
}

//--------------------------------------------------------------------------//

std::string
Formatter::formatLine( const std::string & _data ) const
{;
    return addAlignment( _data );
}

//--------------------------------------------------------------------------//

void
Formatter::addBorders( 
        std::string & _data
    ,   bool          _firstColumn
) const
{
    std::string borderedLine;
    if( _firstColumn )
        borderedLine += m_leftBorder;
    borderedLine += _data;
    borderedLine += m_rightBorder;
    _data = borderedLine;
}

//--------------------------------------------------------------------------//

void
Formatter::addSpaces( std::vector< std::string > & _data ) const
{
    adddelimiters( _data, ' ' );
}

//--------------------------------------------------------------------------//

void
Formatter::addNewLines( std::vector< std::string > & _data ) const
{
    adddelimiters( _data, '\n' );
}

//--------------------------------------------------------------------------//

void
Formatter::adddelimiters( 
        std::vector< std::string > & _data
    ,   const char                   _delim
) const
{
    for( auto & line : _data )
        line += _delim;
}

//--------------------------------------------------------------------------//

std::string
Formatter::dataToString( std::vector< std::string > const & _data ) const
{
    std::string formedString;
    for( auto const & row : _data )
    {
        formedString += row;
    }
    return formedString;
}

//--------------------------------------------------------------------------//

std::string
Formatter::addAlignment( const std::string & _data ) const
{
    std::string formedLine{};
    switch( m_alignment )
    {
        case Property::AlignmentType::Left :
        {
            formedLine += _data;
            int indentLength = m_rowLength - formedLine.length();
            std::string indent( indentLength, ' ' );
            formedLine += indent;
            break;
        }

        case Property::AlignmentType::Center :
        {
            // TODO
            // if the string is longer than width of page it will not work correctly;
            int indentLength = ( m_rowLength / 2 ) - ( _data.length() / 2 );
            std::string indent( indentLength, ' ' );
            formedLine += indent;
            formedLine += _data;

            indentLength = m_rowLength - formedLine.length();
            indent = std::string( indentLength, ' ' );
            formedLine += indent;
            
            break;
        }

        case Property::AlignmentType::Right :
        {
            //TODO optimize it
            int indentLength = m_rowLength - _data.length();
            std::string indent( indentLength, ' ' );
            formedLine += indent;
            formedLine += _data;
            break;
        }   

        default:
        {
            formedLine += _data;
            int indentLength = m_rowLength - formedLine.length();
            std::string indent( indentLength, ' ' );
            formedLine += indent;
            break;
        }
    }
    return formedLine;
}

//--------------------------------------------------------------------------//

//--------------------------------------------------------------------------//

void 
Formatter::setReportWidth( 
    const Property::ReportWidth & _reportWidth
)
{
    m_reportWidth = _reportWidth.get();
}

//--------------------------------------------------------------------------//

void
Formatter::setReportHeight(
    const Property::ReportHeight & _reportHeight
)
{
    m_reportHeight = _reportHeight.get();
}

//--------------------------------------------------------------------------//

void
Formatter::setAlignment(
    const Property::Alignment & _alignment
)
{
    m_alignment = _alignment.get();
}

//--------------------------------------------------------------------------//

void
Formatter::setRowLength(
    const Property::RowLength & _rowLength
)
{
    m_rowLength = _rowLength.get();
}

//--------------------------------------------------------------------------//

void
Formatter::setRowHeight(
    const Property::RowHeight & _rowHeight
)
{
    m_rowHeight = _rowHeight.get();
}

//--------------------------------------------------------------------------//

void
Formatter::setRowNumber(
    const Property::RowNumber & _rowNumber
)
{
    m_rowNumber = _rowNumber.get();
}

//--------------------------------------------------------------------------//

void
Formatter::setColumnNumber(
    const Property::ColumnNumber & _columnNumber
)
{
    m_columnNumber = _columnNumber.get();
}

//--------------------------------------------------------------------------//

void
Formatter::setTopBorder(
    const Property::Border::Top & _topBorder
)
{
    m_topBorder = _topBorder.get();
}

//--------------------------------------------------------------------------//

void
Formatter::setBottomBorder(
    const Property::Border::Bottom & _bottomBorder
)
{
    m_bottomBorder = _bottomBorder.get();
}

//--------------------------------------------------------------------------//

void
Formatter::setLeftBorder(
    const Property::Border::Left & _leftBorder
)
{
    m_leftBorder = _leftBorder.get();
}

//--------------------------------------------------------------------------//

void
Formatter::setRightBorder(
    const Property::Border::Right & _rightBorder
)
{
    m_rightBorder = _rightBorder.get();
}

//--------------------------------------------------------------------------//

int
Formatter::calculateTableHeight( int _dataHeight ) const
{
    // m_rowNumber + 1 stands for the amount of borders
    // borders are treated as strings
    return _dataHeight + m_rowNumber + 1;
}

//--------------------------------------------------------------------------//

int
Formatter::calculateTableLength( int _dataLength ) const
{
    // m_columnNumber + 1 stands for the amount of borders
    // borders are treated as strings
    return _dataLength + m_columnNumber + 1;
}