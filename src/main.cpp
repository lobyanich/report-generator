
#include "container/Report.hpp"

#include "container/LableObject.hpp"
#include "container/HorizontalLayoutObject.hpp"
#include "container/table/TableObject.hpp"

#include "formatter/FormatterImpl.hpp"

int main ()
{

    Formatter formatter;
    Report report( formatter );

    LableObject headerLable0{ 
            "FirstBlock:"
        ,   Property::Alignment( Property::AlignmentType::Left )
        ,   Property::RowLength( 20 )
    };

    LableObject headerLable1{ 
            "SecondBlock:"
        ,   Property::Alignment( Property::AlignmentType::Left )
        ,   Property::RowLength( 20 )
    };

    LableObject headerLable2{ 
            "ThirdBlock:"
        ,   Property::Alignment( Property::AlignmentType::Left )
        ,   Property::RowLength( 20 )
    };

    LableObject headerLable3{ 
            "FourthBlock:"
        ,   Property::Alignment( Property::AlignmentType::Left )
        ,   Property::RowLength( 20 )
    };

    LableObject headerLable4{ 
            "FifthBlock:"
        ,   Property::Alignment( Property::AlignmentType::Left )
        ,   Property::RowLength( 20 )
    };

    HorizontalLayoutObject header(
            headerLable0
        ,   headerLable1
        ,   headerLable2
        ,   headerLable3
        ,   headerLable4
    );

    LableObject firstRowLable0{ 
            "FirstRow\nSecondRow\nThirdRow"
        ,   Property::Alignment( Property::AlignmentType::Right )
        ,   Property::RowLength( 20 )
    };

    LableObject firstRowLable1{ 
            "FirstRow\nSecondRow"
        ,   Property::Alignment( Property::AlignmentType::Left )
        ,   Property::RowLength( 20 )
    };

    LableObject firstRowLable2{ 
            "FirstRow\nSecond Row\nThirdRow\nFourth Row"
        ,   Property::Alignment( Property::AlignmentType::Left )
        ,   Property::RowLength( 20 )
    };

    LableObject firstRowLable3{ 
            "FirstRow\nSecondRow\nThirdRow"
        ,   Property::Alignment( Property::AlignmentType::Right )
        ,   Property::RowLength( 20 )
    };

    LableObject firstRowLable4{ 
            "FirstRow\nSecondRow\nThirdRow\nFourthRow\nFifthRow"
        ,   Property::Alignment( Property::AlignmentType::Left )
        ,   Property::RowLength( 20 )
    };

    HorizontalLayoutObject firstRow(
            firstRowLable0
        ,   firstRowLable1
        ,   firstRowLable2
        ,   firstRowLable3
        ,   firstRowLable4
    );

    LableObject tableName{ 
            "Some table"
        ,   Property::Alignment( Property::AlignmentType::Center )
        ,   Property::RowLength( 137 )
    };

    TableObject table( 3, 6 );

    for( int i{ 0 }; i < 3; ++i )
    {
        for( int j{ 0 }; j < 6; ++j )
        {
            std::string data = "Row";
            data += "\nColumn ";
            Property::Alignment alignment( Property::AlignmentType::Left );

            if( j % 2 != 0 )
            {
                data += "\nSomething";
                data += "\nSomething Else very big";
                alignment = Property::Alignment( Property::AlignmentType::Center );
            }
            LableObject * lable = new LableObject{
                    data
                ,   alignment
                ,   Property::RowLength( 20 )
            };
            table.addLable( 
                    lable
                ,   Property::RowNumber( i )
                ,   Property::ColumnNumber( j )
            );
        }
    }

    report.addRow( header );
    report.addRow( firstRow );
    report.addRow( tableName );
    report.addRow( table );

    report.print();

    return 0;
}