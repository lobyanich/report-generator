#ifndef __TABLE_OBJECT_HPP__
#define __TABLE_OBJECT_HPP__

//--------------------------------------------------------------------------//

#include "interface/TableInterface.hpp"
#include "interface/Property.hpp"
#include <vector>
#include <memory>

//--------------------------------------------------------------------------//

class LableInterface;

//--------------------------------------------------------------------------//

class TableObject
    :   public TableInterface
{
public:

    TableObject( int _rows, int _columns );

    //add constructor from scratch

    std::string accept( ReportObjectVisitor & _visitor ) const override;

    void
    forEachData (
        std::function< void ( const LableInterface & ) > _callbakc
    ) const override;

    int getHeight() const override;
    int getLength() const override;

    int getRows() const override;
    int getColumns() const override;

    int getRowHeight( int _rowIndex ) const override;
    int getColumnWidth( int _columnIndex ) const override;

    void addLable(
            const LableInterface *
        ,   const Property::RowNumber &
        ,   const Property::ColumnNumber &
    );

private:

    void generateWidth();
    void generateHeight();

private:

    int m_height;
    int m_length;

    std::vector< int > m_columnWidth;
    std::vector< int > m_rowHeight;

    int m_rows;
    int m_columns;

    std::vector< const LableInterface * > m_rawData;
    std::vector< std::string > m_formattedData;

};

//--------------------------------------------------------------------------//

inline int
TableObject::getHeight() const
{
    return m_height;
}

//--------------------------------------------------------------------------//

inline int
TableObject::getLength() const
{
    return m_length;
}

//--------------------------------------------------------------------------//

inline int
TableObject::getRows() const
{
    return m_rows;
}

//--------------------------------------------------------------------------//

inline int
TableObject::getColumns() const
{
    return m_columns;
}

//--------------------------------------------------------------------------//

inline int
TableObject::getRowHeight( int _rowIndex ) const
{
    return m_rowHeight.at( _rowIndex );
}

//--------------------------------------------------------------------------//

inline int
TableObject::getColumnWidth( int _columnIndex ) const
{
    return m_columnWidth.at( _columnIndex );
}

//--------------------------------------------------------------------------//

#endif // __TABLE_OBJECT_HPP__