#include "TableObject.hpp"
#include "interface/LableInterface.hpp"

//--------------------------------------------------------------------------//

TableObject::TableObject( int _rows, int _columns )
    :   m_rows( _rows )
    ,   m_columns( _columns )
    ,   m_columnWidth( _columns )
    ,   m_rowHeight( _rows )
    ,   m_rawData( _rows * _columns )
{
}

//--------------------------------------------------------------------------//

void
TableObject::forEachData (
    std::function< void ( const LableInterface & ) > _callbakc
) const
{
    for( auto const & lable : m_rawData )
        _callbakc( *lable );
}

//--------------------------------------------------------------------------//

void
TableObject::addLable(
        const LableInterface *         _lable
    ,   const Property::RowNumber &    _rowNumber
    ,   const Property::ColumnNumber & _columnNumber
)
{
    int cellHeight = m_rowHeight[ _rowNumber.get() ];
    int cellWidth = m_columnWidth[ _columnNumber.get() ];
    int lableHeight = _lable->getHeight();
    int lableWidth = _lable->getLength();

    if( lableWidth > cellWidth )
    {
        m_columnWidth[ _columnNumber.get() ] = lableWidth;
        generateWidth();
    }

    if( lableHeight > cellHeight )
    {
        m_rowHeight[ _rowNumber.get() ] = lableHeight;
        generateHeight();
    }

    int index = _rowNumber.get() * m_columns + _columnNumber.get();
    m_rawData.at( index ) =  _lable;
}

//--------------------------------------------------------------------------//

void
TableObject::generateWidth()
{
    int width{ 0 };
    for( auto columnWidth : m_columnWidth )
        width += columnWidth;
    m_length = width;
}

//--------------------------------------------------------------------------//

void
TableObject::generateHeight()
{
    int height{ 0 };
    for( auto rowHeight : m_rowHeight )
        height += rowHeight;
    m_height = height;
}

//--------------------------------------------------------------------------//

std::string 
TableObject::accept( ReportObjectVisitor & _visitor ) const
{
    return _visitor.visit( *this );
}