#include "LableObject.hpp"
#include "interface/Property.hpp"

#include <sstream>
#include <utility>

//--------------------------------------------------------------------------//

LableObject::LableObject()
{  
}

//--------------------------------------------------------------------------//

LableObject::LableObject( const LableObject & _lable )
    :   m_rawData(_lable.m_rawData )
    ,   m_properties( _lable.m_properties )
{
}

//--------------------------------------------------------------------------//

LableObject::LableObject( LableObject && _lable )
    :   m_rawData( std::move(_lable.m_rawData ))
    ,   m_properties( std::move( _lable.m_properties ))
{
}

//--------------------------------------------------------------------------//

void
LableObject::forEachProperty(
    PropertyCallback _callbakc
) const
{
    for( auto const & property : m_properties )
        _callbakc( property );
}

//--------------------------------------------------------------------------//

void
LableObject::forEachData(
    DataCallback _callbakc
) const
{
    for( auto const & line : m_rawData )
        _callbakc( line );
}

//--------------------------------------------------------------------------//

void
LableObject::processStringData( std::string const & _string )
{
    handleNewLine( _string );
}

//--------------------------------------------------------------------------//

int
LableObject::calculateLength() const
{
    int length{ 0 };

    for( auto const & line : m_rawData )
        if( line.length() > length )
            length = line.length();

    return length;
}

//--------------------------------------------------------------------------//

void
LableObject::handleNewLine( std::string const & _string )
{
    std::string line;

    std::stringbuf stringBuffer;
    stringBuffer.pubsetbuf(
            const_cast< char * >( _string.data() )
        ,   static_cast< std::streamsize >( _string.size() )
    );
    std::istream is( &stringBuffer );

    while ( std::getline( is, line, '\n' ))
        m_rawData.push_back( line );
}

//--------------------------------------------------------------------------//

std::string 
LableObject::accept( ReportObjectVisitor & _visitor ) const
{
    return _visitor.visit( *this );
}