#ifndef __REPORT_HPP__
#define __REPORT_HPP__

//--------------------------------------------------------------------------//

#include <vector>
#include <iostream>
#include <functional>

#include "interface/ReportObject.hpp"
#include "interface/ReportObjectVisitor.hpp"

//--------------------------------------------------------------------------//

//--------------------------------------------------------------------------//

class Report
{

public:

    Report( ReportObjectVisitor & );

    template< typename... _ReportObject >
    void
    addRow( 
        _ReportObject&&...  _reports
    )
    {
        ( void )( int[] )
        {
            ( 
                    m_data.push_back( _reports )
                ,   int{ 0 } 
            )...
        };
    }

    void print( std::ostream & _outStream = std::cout );

private:

    std::vector< std::reference_wrapper< const ReportObject >> m_data;
    ReportObjectVisitor & m_formater;
};

//--------------------------------------------------------------------------//

#endif // __REPORT_HPP__