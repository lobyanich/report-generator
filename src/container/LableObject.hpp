#ifndef __LABLE_OBJECT_HPP__
#define __LABLE_OBJECT_HPP__

//--------------------------------------------------------------------------//

#include <vector>
#include <iostream>
#include <type_traits>

#include "interface/LableInterface.hpp"

//--------------------------------------------------------------------------//
class LableObject
    :   public LableInterface
{
public:

    LableObject();

    LableObject( const LableObject & );
    LableObject( LableObject && );

    template< typename... _PropertyType >
    LableObject( 
            std::string const & _data
        ,   _PropertyType &&... _properties
    )
    {
        processStringData( _data );
        ( void )( int[] )
        {
            ( 
                    m_properties.push_back(
                        new typename 
                            std::remove_reference< _PropertyType && >::type(
                                _properties
                            )
                    )
                ,   int{ 0 } 
            )...
        };
    }

    std::string accept( ReportObjectVisitor & _visitor ) const override;

    int getHeight() const noexcept override;
    int getLength() const override;

    void
    forEachProperty ( 
        PropertyCallback _callbakc
    ) const override;
    
    void
    forEachData (
        DataCallback _callbakc
    ) const override;

private:

    void
    processStringData( std::string const & );

    void
    handleNewLine( std::string const & );

    int calculateLength() const;

private:

    std::vector< BaseProperty * > m_properties;
    std::vector< std::string > m_rawData;
    std::vector< std::string > m_formattedData;
};

//--------------------------------------------------------------------------//

inline int
LableObject::getHeight() const noexcept
{
    return m_rawData.size();
}

//--------------------------------------------------------------------------//

inline int
LableObject::getLength() const
{
    return calculateLength();
}

//--------------------------------------------------------------------------//

#endif // __LABLE_OBJECT_HPP__