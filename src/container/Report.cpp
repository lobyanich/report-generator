#include "Report.hpp"

//--------------------------------------------------------------------------//

Report::Report( ReportObjectVisitor & _reportVisitor )
    :   m_formater( _reportVisitor )
{
}

//--------------------------------------------------------------------------//

void
Report::print( std::ostream & _outStream )
{
    for( auto const & reportObject : m_data )
        _outStream << reportObject.get().accept( m_formater );
}