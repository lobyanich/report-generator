#include "HorizontalLayoutObject.hpp"
#include "interface/LableInterface.hpp"

//--------------------------------------------------------------------------//

HorizontalLayoutObject::HorizontalLayoutObject()
{
}

//--------------------------------------------------------------------------//

void
HorizontalLayoutObject::forEachData(
    std::function< void ( const LableInterface & ) > _callbakc
) const
{
    for( auto const & lable : m_rawData )
        _callbakc( lable );
}

//--------------------------------------------------------------------------//

std::string 
HorizontalLayoutObject::accept( ReportObjectVisitor & _visitor ) const
{
    return _visitor.visit( *this );
}

//--------------------------------------------------------------------------//

void
HorizontalLayoutObject::addLable( const LableInterface & _lable )
{
    m_rawData.push_back( _lable );
}

//--------------------------------------------------------------------------//

int
HorizontalLayoutObject::calculateLength() const
{
    int length{ 0 };

    for( auto const & lable : m_rawData )
        length += lable.get().getLength();

    // add delims as part of lenth equal to the amout of lables - 1
    length += m_rawData.size();
    return --length;
}

//--------------------------------------------------------------------------//

int 
HorizontalLayoutObject::calculateHeight() const
{
    int height{ 0 };

    for( auto const & lable : m_rawData )
        if ( lable.get().getHeight() > height )
            height = lable.get().getHeight();

    return height;
}