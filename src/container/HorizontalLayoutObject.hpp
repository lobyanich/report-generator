#ifndef __HORIZONTAL_LAYOUT_OBJECT_HPP__
#define __HORIZONTAL_LAYOUT_OBJECT_HPP__

//--------------------------------------------------------------------------//

#include "interface/HorizontalLayoutInterface.hpp"
#include <functional>
#include <vector>

//--------------------------------------------------------------------------//

class HorizontalLayoutObject
    :   public HorizontalLayoutInterface
{
public:

    HorizontalLayoutObject();

    template< typename... _LableType >
    HorizontalLayoutObject( 
        _LableType&&...       _lables
    )
    {
        ( void )( int[] )
        {
            (
                    m_rawData.push_back( _lables )
                ,   int{ 0 }
            )...
        };
    }

    std::string accept( ReportObjectVisitor & _visitor ) const override;

    int getHeight() const override;
    int getLength() const override;

    void
    forEachData (
        DataCallback _callbakc
    ) const override;

    void addLable( const LableInterface & );

private:

    int calculateLength() const;
    int calculateHeight() const;

private:

    std::vector< std::reference_wrapper< const LableInterface >> m_rawData;
    std::vector< std::string > m_formattedData;

};

//--------------------------------------------------------------------------//

inline int
HorizontalLayoutObject::getHeight() const
{
    return calculateHeight();
}

//--------------------------------------------------------------------------//

inline int
HorizontalLayoutObject::getLength() const
{
    return calculateLength();
}

//--------------------------------------------------------------------------//

#endif // __HORIZONTAL_LAYOUT_OBJECT_HPP__