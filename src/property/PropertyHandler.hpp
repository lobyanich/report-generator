#ifndef __PROPERTY_HANDLER_HPP__
#define __PROPERTY_HANDLER_HPP__

//--------------------------------------------------------------------------//

#include "interface/Property.hpp"

//--------------------------------------------------------------------------//

class Formatter;

//--------------------------------------------------------------------------//

class PropertyHandler
    :  public PropertyVisitor
{
public:

    PropertyHandler( Formatter & _formatter )
        :   m_formatter{ _formatter }
    {}

    void visit ( const Property::ReportWidth & ) override;
    void visit ( const Property::ReportHeight & );
    void visit ( const Property::RowLength & );
    void visit ( const Property::RowHeight & );
    void visit ( const Property::RowNumber & );
    void visit ( const Property::ColumnNumber & );
    void visit ( const Property::Border::Top & );
    void visit ( const Property::Border::Bottom & );
    void visit ( const Property::Border::Left & );
    void visit ( const Property::Border::Right & );
    void visit ( const Property::Alignment & );

private:

    Formatter & m_formatter;

};

//--------------------------------------------------------------------------//

#endif // __PROPERTY_HANDLER_HPP__