#include "PropertyHandler.hpp"
#include "src/formatter/FormatterImpl.hpp"

//--------------------------------------------------------------------------//

void 
PropertyHandler::visit ( const Property::ReportWidth & _reportWidth )
{
    m_formatter.setReportWidth( _reportWidth );
}

//--------------------------------------------------------------------------//

void 
PropertyHandler::visit ( const Property::ReportHeight & _reportHeight )
{
    m_formatter.setReportHeight( _reportHeight );
}

//--------------------------------------------------------------------------//

void 
PropertyHandler::visit ( const Property::RowLength & _rowLength )
{
    m_formatter.setRowLength( _rowLength );
}

//--------------------------------------------------------------------------//

void 
PropertyHandler::visit ( const Property::RowHeight & _rowHeight )
{
    m_formatter.setRowHeight( _rowHeight );
}

//--------------------------------------------------------------------------//

void 
PropertyHandler::visit ( const Property::RowNumber & _rowNumber )
{
    m_formatter.setRowNumber( _rowNumber );
}

//--------------------------------------------------------------------------//

void 
PropertyHandler::visit ( const Property::ColumnNumber & _columnNumber )
{
    m_formatter.setColumnNumber( _columnNumber );
}

//--------------------------------------------------------------------------//

void 
PropertyHandler::visit ( const Property::Border::Top & _topBorder )
{
    m_formatter.setTopBorder( _topBorder );
}

//--------------------------------------------------------------------------//

void 
PropertyHandler::visit ( const Property::Border::Bottom & _bottomBorder )
{
    m_formatter.setBottomBorder( _bottomBorder );
}

//--------------------------------------------------------------------------//

void 
PropertyHandler::visit ( const Property::Border::Left & _leftBorder )
{
    m_formatter.setLeftBorder( _leftBorder );
}

//--------------------------------------------------------------------------//

void 
PropertyHandler::visit ( const Property::Border::Right & _rightBorder )
{
    m_formatter.setRightBorder( _rightBorder );
}

//--------------------------------------------------------------------------//

void 
PropertyHandler::visit ( const Property::Alignment & _alignment )
{
    m_formatter.setAlignment( _alignment );
}