#ifndef __LABLE_INTERFACE_HPP__
#define __LABLE_INTERFACE_HPP__

//--------------------------------------------------------------------------//

#include "ReportObject.hpp"
#include <functional>

//--------------------------------------------------------------------------//

class BaseProperty;

//--------------------------------------------------------------------------//

class LableInterface
    :   public ReportObject
{
public:

    using DataCallback = std::function< void ( const std::string & ) >;
    using PropertyCallback = std::function< void ( const BaseProperty * ) >;

    virtual ~LableInterface() = default;

    virtual int getHeight() const = 0;
    virtual int getLength() const = 0;

    virtual void
    forEachProperty (
        PropertyCallback _callbakc
    ) const = 0;
    
    virtual void
    forEachData (
        DataCallback _callbakc
    ) const = 0;
};

//--------------------------------------------------------------------------//

#endif // __LABLE_INTERFACE_HPP__