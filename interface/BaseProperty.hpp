#ifndef __BASE_PROPERTY_HPP__
#define __BASE_PROPERTY_HPP__

//--------------------------------------------------------------------------//

class PropertyVisitor;

//--------------------------------------------------------------------------//

class BaseProperty
{
public:
    virtual ~BaseProperty() = default;
    virtual void accept ( PropertyVisitor & _visitor ) const = 0;
};

//--------------------------------------------------------------------------//

#endif // __BASE_PROPERTY_HPP__