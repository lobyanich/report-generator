#ifndef __PROPERTY_HPP__
#define __PROPERTY_HPP__

//--------------------------------------------------------------------------//

#include "PropertyVisitor.hpp"
#include "BaseProperty.hpp"
#include <utility>

//--------------------------------------------------------------------------//

template < typename _PropertyType, typename _Parameter >
class BaseTypedProperty
    :   public BaseProperty
{
public:

    explicit BaseTypedProperty( 
        _PropertyType const& _value = _PropertyType{}
    )
        :   m_value( _value )
    {}

    //----------------------------------------------------------------------//

    explicit BaseTypedProperty( _PropertyType&& _value ) 
        :   m_value( std::move( _value ))
    {}

    //----------------------------------------------------------------------//

    void 
    accept ( PropertyVisitor & _visitor ) const
    {
        using ThisType = BaseTypedProperty< _PropertyType, _Parameter >;
        _visitor.visit(
            static_cast< const ThisType & >( *this )
        );
    }

    //----------------------------------------------------------------------//

    _PropertyType & 
    get()
    { 
        return m_value;
    }

    //----------------------------------------------------------------------//

    _PropertyType const & 
    get() const 
    {
        return m_value; 
    }

    //----------------------------------------------------------------------//

private:

    _PropertyType m_value;

};

//--------------------------------------------------------------------------//

#endif // __PROPERTY_HPP__