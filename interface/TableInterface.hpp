#ifndef __TABLE_INTERFACE_HPP__
#define __TABLE_INTERFACE_HPP__

//--------------------------------------------------------------------------//

#include "ReportObject.hpp"
#include <functional>

//--------------------------------------------------------------------------//

class LableInterface;

//--------------------------------------------------------------------------//

class TableInterface
    :   public ReportObject
{
public:

    using DataCallback = std::function< void ( const LableInterface & ) >;

    virtual ~TableInterface() = default;

    virtual int getHeight() const = 0;
    virtual int getLength() const = 0;

    virtual int getRows() const = 0;
    virtual int getColumns() const = 0;

    virtual int getRowHeight( int _rowIndex ) const = 0;
    virtual int getColumnWidth( int _columnIndex ) const = 0;

    virtual void
    forEachData (
        DataCallback _callbakc
    ) const = 0;
};

//--------------------------------------------------------------------------//

#endif // __TABLE_INTERFACE_HPP__