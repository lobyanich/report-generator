#ifndef __HORIZONTAL_LAYOUT_INTERFACE_HPP__
#define __HORIZONTAL_LAYOUT_INTERFACE_HPP__

//--------------------------------------------------------------------------//

#include "ReportObject.hpp"
#include <functional>

//--------------------------------------------------------------------------//

class LableInterface;

//--------------------------------------------------------------------------//

class HorizontalLayoutInterface
    :   public ReportObject
{
public:

    using DataCallback = std::function< void ( const LableInterface & ) >;

    virtual ~HorizontalLayoutInterface() = default;

    virtual int getHeight() const = 0;
    virtual int getLength() const = 0;

    virtual void
    forEachData (
        DataCallback _callbakc
    ) const = 0;
};

//--------------------------------------------------------------------------//

#endif // __HORIZONTAL_LAYOUT_INTERFACE_HPP__