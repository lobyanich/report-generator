#ifndef __REPORT_OBJECT_VISITOR_HPP__
#define __REPORT_OBJECT_VISITOR_HPP__

//--------------------------------------------------------------------------//

#include <string>

//--------------------------------------------------------------------------//

class LableInterface;
class TableInterface;
class HorizontalLayoutInterface;

//--------------------------------------------------------------------------//

class ReportObjectVisitor
{
public:

    virtual ~ReportObjectVisitor() = default;

    virtual std::string visit ( const LableInterface & _obj ) = 0;
    virtual std::string visit ( const HorizontalLayoutInterface & _obj ) = 0;
    virtual std::string visit ( const TableInterface & _obj ) = 0;
};

//--------------------------------------------------------------------------//

#endif // __REPORT_OBJECT_VISITOR_HPP__