#ifndef __PROPERTY_VISITOR_HPP__
#define __PROPERTY_VISITOR_HPP__

//--------------------------------------------------------------------------//

template < typename _PropertyType, typename _Parameter >
class BaseTypedProperty;

//--------------------------------------------------------------------------//

namespace Property {

    using ReportWidth  = BaseTypedProperty< int, struct ReportWidthParameter >;
    using ReportHeight = BaseTypedProperty< int, struct ReportHeightParameter >;

    enum AlignmentType
    {
            Left
        ,   Center
        ,   Right
    };

    using Alignment = BaseTypedProperty< AlignmentType, struct AlignmentTypeParameter >;

    using RowLength = BaseTypedProperty< int, struct LengthParameter >;
    using RowHeight = BaseTypedProperty< int, struct HeightParameter >;

    // Tables

    using RowNumber = BaseTypedProperty< int, struct RowNumberParameter >;
    using ColumnNumber = BaseTypedProperty< int, struct ColumnNumberParameter >;

namespace Border
{
    using Top    = BaseTypedProperty< char, struct TopParameter >;
    using Bottom = BaseTypedProperty< char, struct BottomParameter >;
    using Left   = BaseTypedProperty< char, struct LeftParameter >;
    using Right  = BaseTypedProperty< char, struct RightParameter >;

} // namespace Border
} // namespace Property

//--------------------------------------------------------------------------//

class PropertyVisitor
{
public:

    virtual ~PropertyVisitor() = default;

    virtual void visit ( const Property::ReportWidth & ) = 0;
    virtual void visit ( const Property::ReportHeight & ) = 0;
    virtual void visit ( const Property::RowLength & ) = 0;
    virtual void visit ( const Property::RowHeight & ) = 0;
    virtual void visit ( const Property::RowNumber & ) = 0;
    virtual void visit ( const Property::ColumnNumber & ) = 0;
    virtual void visit ( const Property::Border::Top & ) = 0;
    virtual void visit ( const Property::Border::Bottom & ) = 0;
    virtual void visit ( const Property::Border::Left & ) = 0;
    virtual void visit ( const Property::Border::Right & ) = 0;
    virtual void visit ( const Property::Alignment & ) = 0;
};

//--------------------------------------------------------------------------//

#endif // __PROPERTY_VISITOR_HPP__