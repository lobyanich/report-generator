#ifndef __REPORT_OBJECT_HPP__
#define __REPORT_OBJECT_HPP__

//--------------------------------------------------------------------------//

#include "ReportObjectVisitor.hpp"

//--------------------------------------------------------------------------//
class ReportObject
{
public:
    virtual ~ReportObject() = default;
    virtual std::string accept ( ReportObjectVisitor & _visitor ) const = 0;
};

//--------------------------------------------------------------------------//

#endif // __REPORT_OBJECT_HPP__